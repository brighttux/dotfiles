# This is a personal dotfiles backup page.

The dotfiles were tracked using the following setup

## Instructions to setup:
1. git init --bare $HOME/dotfiles 
2. add to bashrc/zshrc/etc: alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME' 
3. refresh bash/zshrc/etc
4. config config --local status.showUntrackedFiles no 


## Basic usage example: 
1. config add /path/to/file 
2. config commit -m "A short message" 
3. config push


### Referenced from:
1. https://www.youtube.com/watch?v=tBoLDpTWVOM&fbclid=IwAR1uK4QN5fpn5-BJbm0O97eyz8G7Y2n4b_eYXWd6bDLNenyLk39opnHBkXA
2. https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/
